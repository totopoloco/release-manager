package at.mavila.projects.relm.deployers;

import org.springframework.stereotype.Service;

import at.mavila.projects.relm.rest.model.ServiceItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Deployer {

    public Integer deploy(final ServiceItem serviceItem) {

        log.info("Deploying: {}", serviceItem);

        // TODO: Implement a proper deploy
        return 1;

    }

}