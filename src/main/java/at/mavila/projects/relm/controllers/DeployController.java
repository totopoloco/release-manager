package at.mavila.projects.relm.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import at.mavila.projects.relm.deployers.Deployer;
import at.mavila.projects.relm.rest.api.DeployApi;
import at.mavila.projects.relm.rest.model.ServiceItem;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@Controller
@RequestMapping("/")
@Slf4j
public class DeployController implements DeployApi {

    private final Deployer deployer;

    @Autowired
    public DeployController(final Deployer deployer) {
        this.deployer = deployer;
    }

    @Override
    public ResponseEntity<Integer> deploy(final ServiceItem serviceItem) {

        Optional<ServiceItem> serviceItemOptional = Optional.ofNullable(serviceItem);

        if (serviceItemOptional.isEmpty()) {
            return ResponseEntity.badRequest().body(-1);
        }

        return ResponseEntity.ok(this.deployer.deploy(serviceItemOptional.get()));

    }

}