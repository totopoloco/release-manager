package at.mavila.projects.relm.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import at.mavila.projects.relm.rest.api.ServicesApi;
import at.mavila.projects.relm.rest.model.ServiceItem;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@Controller
@RequestMapping("/")
@Slf4j
public class ServicesController implements ServicesApi {

    
    @Override
    public ResponseEntity<List<ServiceItem>> services(final Optional<Integer> systemVersion) {
        
        if (systemVersion.isEmpty() || systemVersion.get() < 1) {
            return ResponseEntity.badRequest().body(Collections.emptyList());
        }

        log.info("Retrieving data with system version: {}", systemVersion.get());

        // TODO: Get it from DB or any other service
        final ServiceItem serviceItem1 = new ServiceItem();
        serviceItem1.setName("Service A");
        serviceItem1.setVersion(2);

        final ServiceItem serviceItem2 = new ServiceItem();
        serviceItem2.setName("Service B");
        serviceItem2.setVersion(1);


        final List<ServiceItem> serviceItems = new ArrayList<>();
        serviceItems.add(serviceItem1);
        serviceItems.add(serviceItem2);

        return ResponseEntity.ok(serviceItems);

    }

}